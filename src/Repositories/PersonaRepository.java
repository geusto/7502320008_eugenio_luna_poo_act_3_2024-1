package Repositories;

import Dominio.Persona;
import Interfaces.IPersonaRepository;
import java.util.ArrayList;
import java.util.List;


public class PersonaRepository implements IPersonaRepository{

    @Override
    public void agregarPersona(Persona persona) throws Exception {
        if(persona == null){
            throw new Exception("La persona no puede ser nulo");
        } 
        var p = Persona.listaPersona.get(persona.getDni());
        if(p != null){
            throw new Exception("Ya existe una persona con dni: "+persona.getDni());
        }
        Persona.listaPersona.put(persona.getDni(), persona);
    }

    @Override
    public void modificarPersona(Persona persona) throws Exception {
        throw new UnsupportedOperationException("Not supported yet."); 
    }

    @Override
    public Persona buscarPersonaPorDni(String dni) throws Exception {
        if(dni == null || dni.trim().isEmpty()){
            throw new Exception("El dni no puede ser nulo o vacio");
        }
        if (Persona.listaPersona == null || Persona.listaPersona.isEmpty()) {
            throw new Exception("La bd de personas está vacía");
        }
        var persona = Persona.listaPersona.get(dni);
        if (persona == null) {
            throw new Exception("no existe persona con ese dni");
        }
        return persona;
    }

    @Override
    public void eliminarPersona(String dni) throws Exception {
        throw new UnsupportedOperationException("Not supported yet."); 
    }

    @Override
    public List<Persona> listarPersonas() {
        if (Persona.listaPersona == null) {
            return new ArrayList<Persona>();
        }
        return new ArrayList(Persona.listaPersona.values());
    }
    
}
