
package Interfaces;

import Dominio.Persona;
import java.util.List;

/**
 *
 * @author eugen
 */
public interface IPersonaRepository {
    public void agregarPersona (Persona persona) throws Exception;
    public void modificarPersona (Persona persona) throws Exception;
    public Persona buscarPersonaPorDni (String dni) throws Exception;
    public void eliminarPersona (String dni) throws Exception;
    public List<Persona> listarPersonas();
}
