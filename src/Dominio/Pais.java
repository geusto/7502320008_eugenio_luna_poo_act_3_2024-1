package Dominio;

import java.util.HashMap;
import java.util.Map;

/**
 *
 * @author eugen
 */
public class Pais {
    public String codigo;
    public String nombre;
    public static Map<String, Pais> listaPais = new HashMap<>();
    //protected Map<String, Provincia> Provincia;
    

    public Pais(){
    }

    public Pais(String codigo, String nombre) {
        this.codigo = codigo;
        this.nombre = nombre;
    }

    public String getCodigo() {
        return codigo;
    }

    public String getNombre() {
        return nombre;
    }

    public void setCodigo(String codigo) {
        this.codigo = codigo;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public static Map<String, Pais> getListaPais() {
        return listaPais;
    }

    public static void setListaPais(Map<String, Pais> listaPais) {
        Pais.listaPais = listaPais;
    }
    
    @Override
    public String toString() {
        return nombre; // Para que el JComboBox muestre el nombre del país
    }
    
}
