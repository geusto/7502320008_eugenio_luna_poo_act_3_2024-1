package Dominio;

import java.util.HashMap;
import java.util.Map;

/**
 *
 * @author eugen
 */
public class Manzana {
    public String codigo;
    public String nombre;
    public Distrito distrito;
    
    public static Map<String, Distrito> listaDis = new HashMap<>();
    public static Map<String, Manzana> listaMan = new HashMap<>();

    public Manzana() {
    }

    public Manzana(String codigo, String nombre, Distrito distrito) {
        this.codigo = codigo;
        this.nombre = nombre;
        this.distrito = distrito;
    }

    public String getCodigo() {
        return codigo;
    }

    public void setCodigo(String codigo) {
        this.codigo = codigo;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public Distrito getDistrito() {
        return distrito;
    }

    public void setDistrito(Distrito distrito) {
        this.distrito = distrito;
    }

    public static Map<String, Distrito> getListaDis() {
        return listaDis;
    }

    public static void setListaDis(Map<String, Distrito> listaDis) {
        Manzana.listaDis = listaDis;
    }

    public static Map<String, Manzana> getListaMan() {
        return listaMan;
    }

    public static void setListaMan(Map<String, Manzana> listaMan) {
        Manzana.listaMan = listaMan;
    }

    @Override
    public String toString() {
        return nombre; // Para que el JComboBox muestre el nombre de la manzana
    }
    
    
}
