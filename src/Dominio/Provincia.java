package Dominio;

import java.util.HashMap;
import java.util.Map;

/**
 *
 * @author eugen
 */
public class Provincia {
    public String codigo;
    public String nombre;
    public Pais pais;
    
    public static Map<String, Provincia> listaProv = new HashMap<>();
    public static Map<String, Pais> listaPais = new HashMap<>();

    public Provincia(){
    }
    
    public Provincia(String codigo, String nombre, Pais pais) {
        this.codigo = codigo;
        this.nombre = nombre;
        this.pais = pais;
    }

    public String getCodigo() {
        return codigo;
    }

    public String getNombre() {
        return nombre;
    }

    public void setCodigo(String codigo) {
        this.codigo = codigo;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public Pais getPais() {
        return pais;
    }

    public void setPais(Pais pais) {
        this.pais = pais;
    }

    public static Map<String, Provincia> getListaProv() {
        return listaProv;
    }

    public static void setListaProv(Map<String, Provincia> listaProv) {
        Provincia.listaProv = listaProv;
    }
    
    @Override
    public String toString() {
        return nombre; // Para que el JComboBox muestre el nombre de la provincia
    }
    
}
