package Dominio;

import java.util.Date;
import java.util.HashMap;
import java.util.Map;

/**
 *
 * @author eugen
 */
public class Persona {
  public String clave;
  public String dni;
  public String ti;
  public String nombre;
  public String apellido1;
  public String apellido2;
  public String genero;
  public Date fechaNacimiento;
  public int edad;
  public LugarRegistro lugarNacimiento;
  public LugarResidencia lugarResidencia;
  public int estatura;
  public String estadoCivil;
  public String situacionMilitar;
  public String nivelEstudios;
  public String ocupacion;
  public String grupoEtnico;
  public static Persona personaActual ;
  public static Map<String, Persona> listaPersona = new HashMap<>();

    public Persona() {
    }

    public Persona(String clave, String dni, String ti, String nombre, String apellido1, String apellido2, String genero, Date fechaNacimiento, int edad, LugarRegistro lugarNacimiento, LugarResidencia lugarResidencia, int estatura, String estadoCivil, String situacionMilitar, String nivelEstudios, String ocupacion, String grupoEtnico) {
        this.clave = clave;
        this.dni = dni;
        this.ti = ti;
        this.nombre = nombre;
        this.apellido1 = apellido1;
        this.apellido2 = apellido2;
        this.genero = genero;
        this.fechaNacimiento = fechaNacimiento;
        this.edad = edad;
        this.lugarNacimiento = lugarNacimiento;
        this.lugarResidencia = lugarResidencia;
        this.estatura = estatura;
        this.estadoCivil = estadoCivil;
        this.situacionMilitar = situacionMilitar;
        this.nivelEstudios = nivelEstudios;
        this.ocupacion = ocupacion;
        this.grupoEtnico = grupoEtnico;
    }

    public String getNombre() {
        return nombre;
    }

    public String getDni() {
        return dni;
    }

    public String getClave() {
        return clave;
    }

    public void setClave(String clave) {
        this.clave = clave;
    }

    public static Map<String, Persona> getListaPersona() {
        return listaPersona;
    }

    public static void setListaPersona(Map<String, Persona> listaPersona) {
        Persona.listaPersona = listaPersona;
    }

    public String getTi() {
        return ti;
    }

    public String getApellido1() {
        return apellido1;
    }

    public String getApellido2() {
        return apellido2;
    }

    public String getGenero() {
        return genero;
    }

    public Date getFechaNacimiento() {
        return fechaNacimiento;
    }

    public int getEdad() {
        return edad;
    }

    public LugarRegistro getLugarNacimiento() {
        return lugarNacimiento;
    }

    public LugarResidencia getLugarResidencia() {
        return lugarResidencia;
    }

    public int getEstatura() {
        return estatura;
    }

    public String getEstadoCivil() {
        return estadoCivil;
    }

    public String isSituacionMilitar() {
        return situacionMilitar;
    }

    public String getNivelEstudios() {
        return nivelEstudios;
    }

    public String getOcupacion() {
        return ocupacion;
    }

    public String getGrupoEtnico() {
        return grupoEtnico;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public void setDni(String dni) {
        this.dni = dni;
    }

    public void setTi(String ti) {
        this.ti = ti;
    }

    public void setApellido1(String apellido1) {
        this.apellido1 = apellido1;
    }

    public void setApellido2(String apellido2) {
        this.apellido2 = apellido2;
    }

    public void setGenero(String genero) {
        this.genero = genero;
    }

    public void setFechaNacimiento(Date fechaNacimiento) {
        this.fechaNacimiento = fechaNacimiento;
    }

    public void setLugarNacimiento(LugarRegistro lugarNacimiento) {
        this.lugarNacimiento = lugarNacimiento;
    }

    public void setLugarResidencia(LugarResidencia lugarResidencia) {
        this.lugarResidencia = lugarResidencia;
    }

    public void setEdad(int edad) {
        this.edad = edad;
    }

    public void setEstatura(int estatura) {
        this.estatura = estatura;
    }

    public void setEstadoCivil(String estadoCivil) {
        this.estadoCivil = estadoCivil;
    }

    public void setSituacionMilitar(String situacionMilitar) {
        this.situacionMilitar = situacionMilitar;
    }

    public void setNivelEstudios(String nivelEstudios) {
        this.nivelEstudios = nivelEstudios;
    }

    public void setOcupacion(String ocupacion) {
        this.ocupacion = ocupacion;
    }

    public void setGrupoEtnico(String grupoEtnico) {
        this.grupoEtnico = grupoEtnico;
    }
    
    @Override
        public String toString() {
        return nombre; // Para que el JComboBox muestre el nombre de la provincia
    }

}
