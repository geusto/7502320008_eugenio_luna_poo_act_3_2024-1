package Dominio;

import java.util.Date;
import java.util.HashMap;
import java.util.Map;

public class Administrador extends Persona{
    public String codigo;
    
    public static Map<String, Administrador> listaAdmin = new HashMap<>();
    public static Administrador adminActual;

    public Administrador() {
    }
    

    public Administrador(String codigo, String clave, String dni, String ti, String nombre, String apellido1, String apellido2, String genero, Date fechaNacimiento, int edad, LugarRegistro lugarNacimiento, LugarResidencia lugarResidencia, int estatura, String estadoCivil, String situacionMilitar, String nivelEstudios, String ocupacion, String grupoEtnico) {
        super(clave, dni, ti, nombre, apellido1, apellido2, genero, fechaNacimiento, edad, lugarNacimiento, lugarResidencia, estatura, estadoCivil, situacionMilitar, nivelEstudios, ocupacion, grupoEtnico);
    }

    public static Map<String, Administrador> getListaAdmin() {
        return listaAdmin;
    }

    public static void setListaAdmin(Map<String, Administrador> listaAdmin) {
        Administrador.listaAdmin = listaAdmin;
    }

    public String getCodigo() {
        return codigo;
    }

    public void setCodigo(String codigo) {
        this.codigo = codigo;
    }
    
    @Override
    public String toString() {
    return nombre; // Para que el JComboBox muestre el nombre de la provincia
    }
  } 