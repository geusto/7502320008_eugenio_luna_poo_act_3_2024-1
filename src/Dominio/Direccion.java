package Dominio;

import java.time.LocalDate;

/**
 *
 * @author eugen
 */
public class Direccion {
    protected int codigo;
    protected String calle;
    protected String numero;
    protected Manzana manzana;

    public Direccion() {
    }

    public Direccion(int codigo, String calle, String numero, Manzana manzana) {
        this.codigo = codigo;
        this.calle = calle;
        this.numero = numero;
        this.manzana = manzana;
    }

    public int getCodigo() {
        return codigo;
    }

    public void setCodigo(int codigo) {
        this.codigo = codigo;
    }

    public String getCalle() {
        return calle;
    }

    public String getNumero() {
        return numero;
    }

    public Manzana getManzana() {
        return manzana;
    }
    
    public void setCalle(String calle) {
        this.calle = calle;
    }

    public void setNumero(String numero) {
        this.numero = numero;
    }

    public void setManzana(Manzana manzana) {
        this.manzana = manzana;
    }
}
