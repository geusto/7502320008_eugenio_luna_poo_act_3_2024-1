package Dominio;

import java.util.HashMap;
import java.util.Map;

/**
 *
 * @author eugen
 */
public class Distrito {
    public String codigo;
    public String nombre;
    public Municipio municipio;
    public Provincia provincia;
    public Pais pais;
    
    public static Map<String, Distrito> listaDis = new HashMap<>();
    public static Map<String, Municipio> listaMun = new HashMap<>();
    
    public Distrito(){
    }

    public Provincia getProvincia() {
        return provincia;
    }

    public void setProvincia(Provincia provincia) {
        this.provincia = provincia;
    }

    public Pais getPais() {
        return pais;
    }

    public void setPais(Pais pais) {
        this.pais = pais;
    }

    public String getCodigo() {
        return codigo;
    }

    public void setCodigo(String codigo) {
        this.codigo = codigo;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public Municipio getMunicipio() {
        return municipio;
    }

    public void setMunicipio(Municipio municipio) {
        this.municipio = municipio;
    }

    public static Map<String, Distrito> getListaDis() {
        return listaDis;
    }

    public static void setListaDis(Map<String, Distrito> listaDis) {
        Distrito.listaDis = listaDis;
    }

    public static Map<String, Municipio> getListaMun() {
        return listaMun;
    }

    public static void setListaMun(Map<String, Municipio> listaMun) {
        Distrito.listaMun = listaMun;
    }

    @Override
    public String toString() {
        return nombre; // Para que el JComboBox muestre el nombre del distrito
    }
    
    
}
