
package Dominio;

import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 *
 * @author eugen
 */
public class Censo {
    public String codigo;
    public String direccion;
    public Date fechaCreacion;
    public List<Persona> personas;
    public String registrador;
    public Manzana manzana;
    public Distrito distrito;
    public Municipio municipio;
    public Provincia provincia;
    public String persona;
    public Pais pais;
    
    public static Map<String, Censo> listaCenso = new HashMap<>();
    public static Map<String, Pais> listaPais = new HashMap<>();
    public static Map<String, Distrito> listaDis = new HashMap<>();
    public static Map<String, Municipio> listaMun = new HashMap<>();
    public static Map<String, Provincia> listaProv = new HashMap<>();
    public static Map<String, Manzana> listaMan = new HashMap<>();
    public static Map<String, Persona> listaPer = new HashMap<>();
    
    public Censo(){
    }

    public Censo(String codigo, String direccion, Date fechaCreacion, List<Persona> personas, String persona, String registrador, Manzana manzana, Distrito distrito, Municipio municipio, Provincia provincia, Pais pais) {
        this.codigo = codigo;
        this.direccion = direccion;
        this.fechaCreacion = fechaCreacion;
        this.personas = personas;
        this.registrador = registrador;
        this.manzana = manzana;
        this.distrito = distrito;
        this.municipio = municipio;
        this.provincia = provincia;
        this.pais = pais;
        this.persona = persona;
    }

    public String getCodigo() {
        return codigo;
    }

    public String getDireccion() {
        return direccion;
    }

    public void setDireccion(String direccion) {
        this.direccion = direccion;
    }

    public Date getFechaCreacion() {
        return fechaCreacion;
    }

    public void setFechaCreacion(Date fechaCreacion) {
        this.fechaCreacion = fechaCreacion;
    }

    public List<Persona> getPersonas() {
        return personas;
    }

    public void setPersonas(List<Persona> personas) {
        this.personas = personas;
    }

    public String getRegistrador() {
        return registrador;
    }

    public void setRegistrador(String registrador) {
        this.registrador = registrador;
    }

    public Manzana getManzana() {
        return manzana;
    }

    public void setManzana(Manzana manzana) {
        this.manzana = manzana;
    }

    public Distrito getDistrito() {
        return distrito;
    }

    public void setDistrito(Distrito distrito) {
        this.distrito = distrito;
    }

    public Municipio getMunicipio() {
        return municipio;
    }

    public void setMunicipio(Municipio municipio) {
        this.municipio = municipio;
    }

    public Provincia getProvincia() {
        return provincia;
    }

    public void setProvincia(Provincia provincia) {
        this.provincia = provincia;
    }

    public Pais getPais() {
        return pais;
    }

    public void setPais(Pais pais) {
        this.pais = pais;
    }

    public static Map<String, Censo> getListaCenso() {
        return listaCenso;
    }

    public static void setListaCenso(Map<String, Censo> listaCenso) {
        Censo.listaCenso = listaCenso;
    }

    public static Map<String, Pais> getListaPais() {
        return listaPais;
    }

    public static void setListaPais(Map<String, Pais> listaPais) {
        Censo.listaPais = listaPais;
    }

    public static Map<String, Distrito> getListaDis() {
        return listaDis;
    }

    public static void setListaDis(Map<String, Distrito> listaDis) {
        Censo.listaDis = listaDis;
    }

    public static Map<String, Municipio> getListaMun() {
        return listaMun;
    }

    public static void setListaMun(Map<String, Municipio> listaMun) {
        Censo.listaMun = listaMun;
    }

    public static Map<String, Provincia> getListaProv() {
        return listaProv;
    }

    public static void setListaProv(Map<String, Provincia> listaProv) {
        Censo.listaProv = listaProv;
    }

    public static Map<String, Manzana> getListaMan() {
        return listaMan;
    }

    public static void setListaMan(Map<String, Manzana> listaMan) {
        Censo.listaMan = listaMan;
    }

    public static Map<String, Persona> getListaPer() {
        return listaPer;
    }

    public static void setListaPer(Map<String, Persona> listaPer) {
        Censo.listaPer = listaPer;
    }

        
}
