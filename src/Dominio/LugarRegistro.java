package Dominio;

import java.util.Date;
import java.util.List;
import java.time.LocalDate;

/**
 *
 * @author eugen
 */
public class LugarRegistro {
    protected int codigo;
    protected Direccion direccion;
    protected LocalDate fechaResidencia;
    protected List<Persona> personas;


    
    public LugarRegistro(){
    }

    public LugarRegistro(int codigo, Direccion direccion, LocalDate fechaResidencia, List<Persona> personas) {
        this.codigo = codigo;
        this.direccion = direccion;
        this.fechaResidencia = fechaResidencia;
        this.personas = personas;
    }

    public int getCodigo() {
        return codigo;
    }

    public void setCodigo(int codigo) {
        this.codigo = codigo;
    }

    public Direccion getDireccion() {
        return direccion;
    }

    public LocalDate getFechaResidencia() {
        return fechaResidencia;
    }

    public List<Persona> getPersonas() {
        return personas;
    }
    
    public void setDireccion(Direccion direccion) {
        this.direccion = direccion;
    }

    public void setFechaResidencia(LocalDate fechaResidencia) {
        this.fechaResidencia = fechaResidencia;
    }

    public void setPersonas(List<Persona> personas) {
        this.personas = personas;
    }
    

}
