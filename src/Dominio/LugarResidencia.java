package Dominio;

import java.util.Date;
import java.util.List;

/**
 *
 * @author eugen
 */
public class LugarResidencia {
    protected Direccion direccion;
    protected String pais;
    protected Date fechaResidencia;
    protected List<Persona> personas;
    
    
    public LugarResidencia(){
    }

    public LugarResidencia(Direccion direccion, String pais, Date fechaResidencia, List<Persona> personas) {
        this.direccion = direccion;
        this.pais = pais;
        this.fechaResidencia = fechaResidencia;
        this.personas = personas;
    }

    public Direccion getDireccion() {
        return direccion;
    }

    public String getPais() {
        return pais;
    }

    public Date getFechaResidencia() {
        return fechaResidencia;
    }

    public List<Persona> getPersonas() {
        return personas;
    }

    public void setDireccion(Direccion direccion) {
        this.direccion = direccion;
    }

    public void setPais(String pais) {
        this.pais = pais;
    }

    public void setFechaResidencia(Date fechaResidencia) {
        this.fechaResidencia = fechaResidencia;
    }

    public void setPersonas(List<Persona> personas) {
        this.personas = personas;
    }
    
    
}
