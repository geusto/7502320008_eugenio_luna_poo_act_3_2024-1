package Dominio;

import java.util.Date;
import java.util.HashMap;
import java.util.Map;

public class Registrador extends Persona{
    public String codigo;
    public Administrador administrador;
    
    public static Map<String, Registrador> listaRegis = new HashMap<>();
    public static Map<String, Administrador> listaAdmin = new HashMap<>();
    public static Registrador regisActual;

    public Registrador() {
    }

    public Registrador(String clave, String dni, String ti, String nombre, String apellido1, String apellido2, String genero, Date fechaNacimiento, int edad, LugarRegistro lugarNacimiento, LugarResidencia lugarResidencia, int estatura, String estadoCivil, String situacionMilitar, String nivelEstudios, String ocupacion, String grupoEtnico) {
        super(clave, dni, ti, nombre, apellido1, apellido2, genero, fechaNacimiento, edad, lugarNacimiento, lugarResidencia, estatura, estadoCivil, situacionMilitar, nivelEstudios, ocupacion, grupoEtnico);
    }

    public Registrador(String codigo, Administrador administrador) {
        this.codigo = codigo;
        this.administrador = administrador;
    }

    public String getCodigo() {
        return codigo;
    }

    public void setCodigo(String codigo) {
        this.codigo = codigo;
    }

    public Administrador getAdminisitrador() {
        return administrador;
    }

    public void setAdminisitrador(Administrador administrador) {
        this.administrador = administrador;
    }

    public static Map<String, Registrador> getListaRegis() {
        return listaRegis;
    }

    public static void setListaRegis(Map<String, Registrador> listaRegis) {
        Registrador.listaRegis = listaRegis;
    }

    public static Map<String, Administrador> getListaAdmin() {
        return listaAdmin;
    }

    public static void setListaAdmin(Map<String, Administrador> listaAdmin) {
        Registrador.listaAdmin = listaAdmin;
    }

    @Override
    public String toString() {
    return nombre; // Para que el JComboBox muestre el nombre de la provincia
    }

   }