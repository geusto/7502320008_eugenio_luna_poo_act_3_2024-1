package Dominio;

import java.util.HashMap;
import java.util.Map;

/**
 *
 * @author eugen
 */
public class Municipio {

    public String codigo;
    public String nombre;
    public Provincia provincia;
    public Pais pais;
    
    public static Map<String, Provincia> listaProv = new HashMap<>();
    public static Map<String, Municipio> listaMun = new HashMap<>();
    
    public Municipio(){
    }

    public Municipio(String codigo, String nombre, Provincia provincia, Pais pais) {
        this.codigo = codigo;
        this.nombre = nombre;
        this.provincia = provincia;
        this.pais = pais;
    }

    public String getCodigo() {
        return codigo;
    }

    public void setCodigo(String codigo) {
        this.codigo = codigo;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public Provincia getProvincia() {
        return provincia;
    }

    public void setProvincia(Provincia provincia) {
        this.provincia = provincia;
    }

    public Pais getPais() {
        return pais;
    }

    public void setPais(Pais pais) {
        this.pais = pais;
    }

    public static Map<String, Provincia> getListaProv() {
        return listaProv;
    }

    public static void setListaProv(Map<String, Provincia> listaProv) {
        Municipio.listaProv = listaProv;
    }

    public static Map<String, Municipio> getListaMun() {
        return listaMun;
    }

    public static void setListaMun(Map<String, Municipio> listaMun) {
        Municipio.listaMun = listaMun;
    }

    @Override
    public String toString() {
        return nombre; // Para que el JComboBox muestre el nombre del municipio
    }
    
}
