
import GUI.VentanaPrincipal;


public class Principal {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        VentanaPrincipal ventanaPrincipal = new VentanaPrincipal();
        ventanaPrincipal.setExtendedState(VentanaPrincipal.MAXIMIZED_BOTH);
        ventanaPrincipal.setVisible(true);
    }
    
}
